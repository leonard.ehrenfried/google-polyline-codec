# google-polyline-codec

[![Maven Central](https://maven-badges.herokuapp.com/maven-central/io.leonard/google-polyline-codec/badge.svg)](https://maven-badges.herokuapp.com/maven-central/io.leonard/google-polyline-codec)


A Java implementation of decoding and encoding polylines using the 
[Google Encoded Polyline Algorithm Format](https://developers.google.com/maps/documentation/utilities/polylinealgorithm).

This is a just a copy of the Mapbox implementation extracted out of their
[Java Services](https://github.com/mapbox/mapbox-java) repository and 
then published as an individual JAR. This is useful if all you
need is a simple polyline encoder/decoder and nothing more.

The project is deployed to Maven Central and you can include it with

```xml
<dependency>
  <groupId>io.leonard</groupId>
  <artifactId>google-polyline-codec</artifactId>
  <version>${version}</version>
</dependency>
```

or 

```
compile 'io.leonard:google-polyline-codec:${version}'
```

## Usage

*Encoding*
```java
final List<Position> path = new ArrayList<>();
path.add(Position.fromLngLat(0, 0));
path.add(Position.fromLngLat(10, 0));
String encoded = PolylineUtils.encode(path, Constants.PRECISION_6);
```

*Decoding*

```java
PolyLineUtils.decode("_cqeFf~cjVf@p@fA}AtAoB`ArAx@hA`GbIvDiFv@gAh@t@X\\|@z@`@Z\\Xf@Vf@VpA\\tATJ@NBBkC", Constants.PRECISION_5);

```

import static io.leonard.PolylineUtils.decode;
import static io.leonard.PolylineUtils.encode;
import static io.leonard.PolylineUtils.simplify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import io.leonard.Position;
import io.leonard.PolylineUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PolylineUtilsTest extends TestUtils {

  // Delta for Coordinates comparison
  private static final double DELTA = 0.000001;

  private static final String SIMPLIFICATION_INPUT = "simplification-input";
  private static final String SIMPLIFICATION_EXPECTED_OUTPUT = "simplification-expected-output";

  private static final String TEST_LINE
    = "_cqeFf~cjVf@p@fA}AtAoB`ArAx@hA`GbIvDiFv@gAh@t@X\\|@z@`@Z\\Xf@Vf@VpA\\tATJ@NBBkC";

  private static final  String TEST_LINE6 =
    "qn_iHgp}LzCy@xCsAsC}PoEeD_@{A@uD_@Sg@Je@a@I_@FcAoFyGcCqFgQ{L{CmD";



  @Test
  public void testDecodePath() {
    List<Position> latLngs = decode(TEST_LINE, Constants.PRECISION_5);

    int expectedLength = 21;
    assertEquals("Wrong length.", expectedLength, latLngs.size());

    Position lastPoint = latLngs.get(expectedLength - 1);
    expectNearNumber(37.76953, lastPoint.getLatitude(), 1e-6);
    expectNearNumber(-122.41488, lastPoint.getLongitude(), 1e-6);
  }

  @Test
  public void testEncodePath5() {
    List<Position> path = decode(TEST_LINE, Constants.PRECISION_5);
    String encoded = encode(path, Constants.PRECISION_5);
    assertEquals(TEST_LINE, encoded);
  }

  @Test
  public void testDecodeEncodePath6() {
    List<Position> path = decode(TEST_LINE6, Constants.PRECISION_6);
    String encoded = PolylineUtils.encode(path, Constants.PRECISION_6);
    assertEquals(TEST_LINE6, encoded);
  }

  @Test
  public void testEncodeDecodePath6() {
    List<Position> originalPath = Arrays.asList(
      Position.fromLngLat(2.2862036, 48.8267868),
      Position.fromLngLat(2.4, 48.9)
    );

    String encoded = encode(originalPath, Constants.PRECISION_6);
    List<Position> path =  decode(encoded, Constants.PRECISION_6);
    assertEquals(originalPath.size(), path.size());

    for (int i = 0; i < originalPath.size(); i++) {
      assertEquals(originalPath.get(i).getLatitude(), path.get(i).getLatitude(), DELTA);
      assertEquals(originalPath.get(i).getLongitude(), path.get(i).getLongitude(), DELTA);
    }
  }


  @Test
  public void decode_neverReturnsNullButRatherAnEmptyList() throws Exception {
    List<Position> path = decode("", Constants.PRECISION_5);
    assertNotNull(path);
    assertEquals(0, path.size());
  }

  @Test
  public void encode_neverReturnsNull() throws Exception {
    String encodedString = encode(new ArrayList<Position>(), Constants.PRECISION_6);
    assertNotNull(encodedString);
  }

  @Test
  public void simplify_neverReturnsNullButRatherAnEmptyList() throws Exception {
    List<Position> simplifiedPath = simplify(new ArrayList<Position>(), Constants.PRECISION_6);
    assertNotNull(simplifiedPath);
  }

  @Test
  public void simplify_returnSameListWhenListSizeIsLessThanOrEqualToTwo(){
    final List<Position> path = new ArrayList<>();
    path.add(Position.fromLngLat(0, 0));
    path.add(Position.fromLngLat(10, 0));
    List<Position> simplifiedPath = simplify(path, Constants.PRECISION_6, true);
    assertTrue("Returned list is different from input list", path == simplifiedPath);
  }

  @Test
  public void simplify_withHighestQuality() throws IOException{
    List<Position> path = createPointListFromResourceFile(SIMPLIFICATION_INPUT);
    List<Position> simplifiedPath = simplify(path, Constants.PRECISION_5, true);
    List<Position> expectedSimplifiedPath = createPointListFromResourceFile(SIMPLIFICATION_EXPECTED_OUTPUT);
    assertEquals("Wrong number of points retained", simplifiedPath.size(), expectedSimplifiedPath.size());
    int counter = 0;
    for(Position retainedPoint:simplifiedPath){
      Position expectedPoint = expectedSimplifiedPath.get(counter);
      assertEquals("Wrong point retained by simplification algorithm", retainedPoint, expectedPoint);
      ++counter;
    }
  }

  private List<Position> createPointListFromResourceFile(String fileName) throws IOException {
    String inputPoints = loadJsonFixture(fileName);
    String[] coords = inputPoints.split(",");

    final List<Position> pointList = new ArrayList<>();
    for(int i= 0;i <= coords.length-2;i = i+2) {
      double x = Double.parseDouble(coords[i]);
      double y = Double.parseDouble(coords[i+1]);
      pointList.add(Position.fromLngLat(x, y));
    }

    return pointList;
  }
}

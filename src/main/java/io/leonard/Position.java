package io.leonard;

import java.util.Objects;

public class Position {
  private double latitude;
  private double longitude;

  public static Position fromLngLat(double lng, double lat) {
    return new Position(lat, lng);
  }

  private Position(double lat, double lng) {
    this.latitude = lat;
    this.longitude = lng;
  }

  public double getLongitude() {
    return longitude;
  }

  public double getLatitude() {
    return latitude;
  }

  @Override
  public String toString() {
    return "Position{" +
      "getLatitude=" + latitude +
      ", getLongitude=" + longitude +
      '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Position point = (Position) o;
    return Double.compare(point.latitude, latitude) == 0 &&
      Double.compare(point.longitude, longitude) == 0;
  }

  @Override
  public int hashCode() {
    return Objects.hash(latitude, longitude);
  }
}
